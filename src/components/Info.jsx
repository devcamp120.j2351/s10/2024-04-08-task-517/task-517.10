import { Component } from "react";

class Info extends Component{
    render () {
        return (
            <p>My name is {this.props.firstName} {this.props.lastName} and my favourite number is {this.props.favNumber}</p>
        )
    }
}

export default Info;